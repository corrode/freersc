package com.openrsc.server.plugins.custom.misc;

import com.openrsc.server.constants.IronmanMode;
import com.openrsc.server.constants.ItemId;
import com.openrsc.server.constants.Quests;
import com.openrsc.server.content.DropTable;
import com.openrsc.server.model.container.Item;
import com.openrsc.server.model.entity.player.Player;
import com.openrsc.server.model.entity.update.ChatMessage; // TODO: this should likely be removed in favour of more authentic quest-style dialogue
import com.openrsc.server.plugins.triggers.OpInvTrigger;
import com.openrsc.server.plugins.triggers.UsePlayerTrigger;
import com.openrsc.server.util.rsc.DataConversions;
import com.openrsc.server.util.rsc.MessageType;

import java.util.ArrayList;

import static com.openrsc.server.plugins.Functions.*;

public class Present implements UsePlayerTrigger, OpInvTrigger {

	private static DropTable cabbagePresentDrops;
	private static DropTable openRSCPresentDrops;

	static {
		DropTable raresTable = new DropTable();
		DropTable runeItemsTable = new DropTable();

		raresTable.addItemDrop(ItemId.PUMPKIN.id(), 1, 1, false);
		raresTable.addItemDrop(ItemId.DISK_OF_RETURNING.id(), 1, 1, false);
		raresTable.addItemDrop(ItemId.CHRISTMAS_CRACKER.id(), 1, 1, false);
		raresTable.addItemDrop(ItemId.EASTER_EGG.id(), 1, 1, false);
		raresTable.addItemDrop(ItemId.BLUE_HALLOWEEN_MASK.id(), 1, 1, false);
		raresTable.addItemDrop(ItemId.GREEN_HALLOWEEN_MASK.id(), 1, 1, false);
		raresTable.addItemDrop(ItemId.RED_HALLOWEEN_MASK.id(), 1, 1, false);
		raresTable.addItemDrop(ItemId.SANTAS_HAT.id(), 1, 1, false);
		raresTable.addItemDrop(ItemId.MAP_PIECE_1.id(), 1, 1, false);

		runeItemsTable.addItemDrop(ItemId.RUNE_LONG_SWORD.id(), 1, 1, false);
		runeItemsTable.addItemDrop(ItemId.RUNE_2_HANDED_SWORD.id(), 1, 1, false);
		runeItemsTable.addItemDrop(ItemId.RUNE_BATTLE_AXE.id(), 1, 1, false);
		runeItemsTable.addItemDrop(ItemId.RUNE_MACE.id(), 1, 1, false);
		runeItemsTable.addItemDrop(ItemId.LARGE_RUNE_HELMET.id(), 1, 1, false);
		runeItemsTable.addItemDrop(ItemId.RUNE_DAGGER.id(), 1, 1, false);
		runeItemsTable.addItemDrop(ItemId.RUNE_SHORT_SWORD.id(), 1, 1, false);
		runeItemsTable.addItemDrop(ItemId.RUNE_SCIMITAR.id(), 1, 1, false);
		runeItemsTable.addItemDrop(ItemId.MEDIUM_RUNE_HELMET.id(), 1, 1, false);
		runeItemsTable.addItemDrop(ItemId.RUNE_CHAIN_MAIL_BODY.id(), 1, 1, false);
		runeItemsTable.addItemDrop(ItemId.RUNE_PLATE_MAIL_BODY.id(), 1, 1, false);
		runeItemsTable.addItemDrop(ItemId.RUNE_PLATE_MAIL_LEGS.id(), 1, 1, false);
		runeItemsTable.addItemDrop(ItemId.RUNE_SQUARE_SHIELD.id(), 1, 1, false);
		runeItemsTable.addItemDrop(ItemId.RUNE_KITE_SHIELD.id(), 1, 1, false);
		runeItemsTable.addItemDrop(ItemId.RUNE_AXE.id(), 1, 1, false);
		runeItemsTable.addItemDrop(ItemId.RUNE_SKIRT.id(), 1, 1, false);
		runeItemsTable.addItemDrop(ItemId.RUNE_PLATE_MAIL_TOP.id(), 1, 1, false);
		runeItemsTable.addItemDrop(ItemId.RUNE_THROWING_KNIFE.id(), 1, 1, false);
		runeItemsTable.addItemDrop(ItemId.RUNE_PICKAXE.id(), 1, 1, false);

		openRSCPresentDrops = new DropTable();
		openRSCPresentDrops.addTableDrop(raresTable, 20);
		openRSCPresentDrops.addTableDrop(runeItemsTable, 80);
	}


	@Override
	public void onUsePlayer(Player player, Player otherPlayer, Item item) {
		/*
		if (item.getCatalogId() == ItemId.PRESENT.id()) {
			// prevent player from using present on ironmen
			if (otherPlayer.isIronMan(IronmanMode.Ironman.id()) || otherPlayer.isIronMan(IronmanMode.Ultimate.id())
				|| otherPlayer.isIronMan(IronmanMode.Hardcore.id()) || otherPlayer.isIronMan(IronmanMode.Transfer.id())) {
				player.playerServerMessage(MessageType.QUEST, otherPlayer.getUsername() + " is an Ironman. " + (otherPlayer.isMale() ? "He" : "She") + " stands alone.");
				return;
			}

			// prevent player from using present on themselves
			if(!config().CAN_USE_CRACKER_ON_SELF && !player.isAdmin() && player.getCurrentIP().equalsIgnoreCase(otherPlayer.getCurrentIP())) {
				player.playerServerMessage(MessageType.QUEST, otherPlayer.getUsername() + " does not want your present...");
				return;
			}

			// prevent player from using present on QoL opt out accounts, accounts who have purposely avoided inauthentic features
			if (otherPlayer.getQolOptOut()) {
				player.playerServerMessage(MessageType.QUEST, otherPlayer.getUsername() + " does not want your present...");
				player.playerServerMessage(MessageType.QUEST, "They have opted out of new features which are inauthentic.");
				return;
			}

			player.face(otherPlayer);
			otherPlayer.face(player);

			thinkbubble(item);

			player.getCarriedItems().remove(item);

			if (config().WANT_EQUIPMENT_TAB) { // TODO: this is not a very good way to detect Cabbage server config
				cabbageRollAndAwardPresent(player, otherPlayer);
			} else { // NOT cabbage config
				openRSCRollAndAwardPresent(player, otherPlayer);
			}
		}*/
	}

	@Override
	public void onOpInv(Player player, Integer invIndex, Item item, String command) {
		if (player.isIronMan(IronmanMode.Ironman.id()) || player.isIronMan(IronmanMode.Ultimate.id())
			|| player.isIronMan(IronmanMode.Hardcore.id())) {

			String playerDialogue;
			if (player.isMale()) {
				playerDialogue = "I am an ironman, I stand alone.";
			} else {
				playerDialogue = "I am an ironwoman, I stand alone.";
			}
			player.getUpdateFlags().setChatMessage(new ChatMessage(player, playerDialogue, null));
			delay(2);
			thinkbubble(item);
			player.playerServerMessage(MessageType.QUEST, "You rip open the present and thrust your hand inside...");

			if (config().WANT_EQUIPMENT_TAB) { // TODO: this is not a very good way to detect Cabbage server config
				//cabbageRollAndAwardPresent(player);
			} else {
				// this code is usually unreachable, since no other official config has ironman mode enabled
				openRSCRollAndAwardPresent(player);
			}
			player.getCarriedItems().remove(item);

		} else {
			openRSCRollAndAwardPresent(player, null, true);
		}
	}


	@Override
	public boolean blockUsePlayer(Player player, Player otherPlayer, Item item) {
		return item.getCatalogId() == ItemId.PRESENT.id();
	}

	@Override
	public boolean blockOpInv(Player player, Integer invIndex, Item item, String command) {
		return item.getCatalogId() == ItemId.PRESENT.id();
	}

	/*
	private void cabbageRollAndAwardPresent(Player player, Player otherPlayer, boolean selfUse) {
		if (!selfUse) {
			player.playerServerMessage(MessageType.QUEST, "You give a present to " + otherPlayer.getUsername());
			otherPlayer.playerServerMessage(MessageType.QUEST, player.getUsername() + " handed you a present...");
			delay();
			otherPlayer.playerServerMessage(MessageType.QUEST, "You unwrap the present and reach your hand inside...");
			delay();
		}

		ArrayList<Item> prizeList = cabbagePresentDrops.rollItem(false, otherPlayer);
		if (prizeList.size() <= 0) return;
		Item prize = prizeList.get(0);
		String prizeName = prize.getDef(player.getWorld()).getName().toLowerCase();

		if (!selfUse)
			player.playerServerMessage(MessageType.QUEST, otherPlayer.getUsername() + " got a " + prizeName + " from your present!");
		otherPlayer.playerServerMessage(MessageType.QUEST, "You take out a " + prizeName + ".");
		delay();

		String playerDialogue;

		if (prize.getCatalogId() == ItemId.COAL.id()) {
			switch (DataConversions.random(0, 8)) {
				default:
				case 0:
					playerDialogue = "No presents for you!";
					break;
				case 1:
					playerDialogue = "Naughty boys and girls get coal for christmas";
					break;
				case 2:
					playerDialogue = "Oh, behave!";
					break;
				case 3:
					playerDialogue = "I can get you off the Naughty List, for a price...";
					break;
				case 4:
					playerDialogue = "Darn! Almost had it.";
					break;
				case 5:
					playerDialogue = "a glitch for a grinch, a pile of coal for you!";
					break;
				case 6:
					playerDialogue = "For not believing in Christmas you get a lump of coal";
					break;
				case 7:
					playerDialogue = "for behavior so cold, you are getting a lump of coal";
					break;
				case 8:
					playerDialogue = "I know what you did last summer";
					break;
			}
		} else {
			playerDialogue = "Happy holidays";
		}

		if (!selfUse)
			player.getUpdateFlags().setChatMessage(new ChatMessage(player, playerDialogue, null));
		otherPlayer.getCarriedItems().getInventory().add(prize);
	}

	private void cabbageRollAndAwardPresent(Player player, Player otherPlayer) {
		cabbageRollAndAwardPresent(player, otherPlayer, false);
	}

	private void cabbageRollAndAwardPresent(Player player) {
		cabbageRollAndAwardPresent(player, player, true);
	}*/

	private void openRSCRollAndAwardPresent(Player player, Player otherPlayer, boolean selfUse) {
		ArrayList<Item> prizeList = openRSCPresentDrops.rollItem(false, player);
		if (prizeList.size() <= 0) return;
		Item prize = prizeList.get(0);
		String prizeName = prize.getDef(player.getWorld()).getName().toLowerCase();

		int unwrapDelay = 2;
		int readingDelay = 2;

		/*
		if (!selfUse) {
			player.playerServerMessage(MessageType.QUEST, "You give a present to " + otherPlayer.getUsername());
			otherPlayer.playerServerMessage(MessageType.QUEST, player.getUsername() + " handed you a present...");
			delay();

			if (prize.getDef(player.getWorld()).getId() == ItemId.KITTEN.id() && otherPlayer.getQuestStage(Quests.GERTRUDES_CAT) != -1) {
				// kitten selected, but otherPlayer ineligible to receive it, swamp toad substituted
				player.playerServerMessage(MessageType.QUEST, "You hope they enjoy the swamp toad you got them!");
			} else if ((prize.getDef(player.getWorld()).getId() >= ItemId.BOOTS_PINK.id() && prize.getDef(player.getWorld()).getId() <= ItemId.BOOTS_BLUE.id())
				|| prize.getDef(player.getWorld()).getId() == ItemId.DESERT_BOOTS.id()) {
				player.playerServerMessage(MessageType.QUEST, "You hope they enjoy the socks you got them!");
			} else {
				player.playerServerMessage(MessageType.QUEST, "You hope they enjoy the " + prizeName + " you got them!");
			}

			switch (ItemId.getById(prize.getDef(player.getWorld()).getId())) {


				case KITTEN:
					if (otherPlayer.getQuestStage(Quests.GERTRUDES_CAT) == -1) { // has completed quest requirement
						otherPlayer.playerServerMessage(MessageType.QUEST, "As you unwrap the present, you hear a mewing noise...!!");
						delay(unwrapDelay);
						player.playerServerMessage(MessageType.QUEST, "@yel@" + otherPlayer.getUsername() + ": oh my gosh a kitten!!?!");
						otherPlayer.getCarriedItems().getInventory().add(prize);
						say(otherPlayer, "oh my gosh a kitten!!?!");
						otherPlayer.playerServerMessage(MessageType.QUEST, "@yel@" + player.getUsername() + ": yeah, I hope you enjoy your new pet and take good care of them!");
						say(player, "yeah, I hope you enjoy your new pet and take good care of them!");

						break;
					} else {
						prize = new Item(ItemId.SWAMP_TOAD.id());
						// fall through to swamp toad case
					}
				case SWAMP_TOAD:
					otherPlayer.playerServerMessage(MessageType.QUEST, "As you unwrap the present, you hear a croaking noise...!!");
					delay(unwrapDelay);
					player.playerServerMessage(MessageType.QUEST, "@yel@" + otherPlayer.getUsername() + ": oh my gosh a toad!!?!");
					otherPlayer.getCarriedItems().getInventory().add(prize);
					say(otherPlayer, "oh my gosh a toad!!?!");
					otherPlayer.playerServerMessage(MessageType.QUEST, "@yel@" + player.getUsername() + ": yeah, I hope you enjoy your new pet and take good care of them!");
					say(player, "yeah, I hope you enjoy your new pet and take good care of them!");
					player.playerServerMessage(MessageType.QUEST, "@red@Server Message: @whi@please do not viciously dismember your new pet toad");
					otherPlayer.playerServerMessage(MessageType.QUEST, "@red@Server Message: @whi@please do not viciously dismember your new pet toad");
					break;

				case CABBAGE:
					otherPlayer.playerServerMessage(MessageType.QUEST, "As you unwrap the present, you can smell something weird...");
					delay(unwrapDelay);
					otherPlayer.playerServerMessage(MessageType.QUEST, "it's a cabbage...!!!");
					otherPlayer.getCarriedItems().getInventory().add(prize);
					break;

				case CHOC_CRUNCHIES:
				case SPICE_CRUNCHIES:
				case CHOCOLATE_SLICE:
					otherPlayer.playerServerMessage(MessageType.QUEST, "You unwrap the present...");
					delay(unwrapDelay);
					if (prize.getDef(player.getWorld()).getId() == ItemId.CHOCOLATE_SLICE.id()) {
						otherPlayer.playerServerMessage(MessageType.QUEST, "Awh, it's some really nice homemade chocolate cake!");
					} else {
						otherPlayer.playerServerMessage(MessageType.QUEST, "Awh, it's some really nice homemade " + prizeName + "!");
					}
					otherPlayer.getCarriedItems().getInventory().add(prize);
					delay(readingDelay);
					otherPlayer.playerServerMessage(MessageType.QUEST, "and it looks like there's also an entire bucket of milk inside!");
					otherPlayer.getCarriedItems().getInventory().add(new Item(ItemId.MILK.id()));
					break;

				case UGTHANKI_KEBAB:
				case TASTY_UGTHANKI_KEBAB:
					otherPlayer.playerServerMessage(MessageType.QUEST, "As you unwrap the present, you can smell something weird...");
					delay(unwrapDelay);
					otherPlayer.playerServerMessage(MessageType.QUEST, "it's an ugthanki kebab!!");
					otherPlayer.getCarriedItems().getInventory().add(prize);
					break;

				case BREAD_DOUGH:
					otherPlayer.playerServerMessage(MessageType.QUEST, "You unwrap the present...");
					delay(unwrapDelay);
					otherPlayer.playerServerMessage(MessageType.QUEST, "Ah! it's an Amish Friendship Bread starter...!");
					otherPlayer.getCarriedItems().getInventory().add(prize);
					delay(readingDelay);
					otherPlayer.playerServerMessage(MessageType.QUEST, "You're supposed to break off a piece to act as a starter yeast, and bake the rest.");
					delay(readingDelay);
					otherPlayer.playerServerMessage(MessageType.QUEST, "Take the piece you saved and use more flour & water to create volume");
					delay(readingDelay);
					otherPlayer.playerServerMessage(MessageType.QUEST, "The yeast should grow over time if you feed it sugar,");
					delay(readingDelay);
					otherPlayer.playerServerMessage(MessageType.QUEST, "and then you can pass it on to a friend :-)");
					break;


				case BRANDY:
				case WHISKY:
				case VODKA:
				case GIN:
					otherPlayer.playerServerMessage(MessageType.QUEST, "You unwrap the present...");
					delay(unwrapDelay);
					otherPlayer.playerServerMessage(MessageType.QUEST, "Oh, nice! It's some gnome " + prizeName + "!");
					otherPlayer.getCarriedItems().getInventory().add(prize);
					break;
				case POISON_CHALICE:
					otherPlayer.playerServerMessage(MessageType.QUEST, "You unwrap the present...");
					delay(unwrapDelay);
					otherPlayer.playerServerMessage(MessageType.QUEST, "... it's some kind of strange cocktail of random spirits!");
					// see https://twitter.com/JagexAsh/status/1073671447753711616 for more info on poison chalice
					otherPlayer.getCarriedItems().getInventory().add(prize);
					break;


				case OYSTER_PEARL_BOLT_TIPS:
					otherPlayer.playerServerMessage(MessageType.QUEST, "You unwrap the present...");
					delay(unwrapDelay);
					otherPlayer.playerServerMessage(MessageType.QUEST, "Ooh! It's some pointed pearls!");
					otherPlayer.getCarriedItems().getInventory().add(prize);
					break;
				case GNOME_BALL:
					otherPlayer.playerServerMessage(MessageType.QUEST, "You unwrap the present...");
					delay(unwrapDelay);
					otherPlayer.playerServerMessage(MessageType.QUEST, "Oh, fun! A gnome ball! I always wanted one of those");
					otherPlayer.getCarriedItems().getInventory().add(prize);
					break;
				case PARAMAYA_REST_TICKET:
					otherPlayer.playerServerMessage(MessageType.QUEST, "You unwrap the present...");
					delay(unwrapDelay);
					otherPlayer.playerServerMessage(MessageType.QUEST, "It's a gift card for a free stay in the Paramaya Inn!");
					otherPlayer.getCarriedItems().getInventory().add(prize);
					if (otherPlayer.getQuestStage(Quests.SHILO_VILLAGE) != -1) {
						// otherPlayer is currently unable to access the inn, located inside shilo village
						delay(readingDelay + 1);
						otherPlayer.playerServerMessage(MessageType.QUEST, "... Wonder where that is?");
					}
					break;
				case SHIP_TICKET:
					otherPlayer.playerServerMessage(MessageType.QUEST, "You unwrap the present...");
					delay(unwrapDelay);
					otherPlayer.playerServerMessage(MessageType.QUEST, "WOW!! it's a ticket for @mag@a trip on a cruise ship!!!!");
					otherPlayer.getCarriedItems().getInventory().add(prize);
					break;


				case GNOME_ROBE_PINK: // gnome robe skirts
				case GNOME_ROBE_GREEN:
				case GNOME_ROBE_PURPLE:
				case GNOME_ROBE_CREAM:
				case GNOME_ROBE_BLUE:
					otherPlayer.playerServerMessage(MessageType.QUEST, "You unwrap the present...");
					delay(unwrapDelay);
					otherPlayer.playerServerMessage(MessageType.QUEST, "it's a very nice pastel dress");
					otherPlayer.getCarriedItems().getInventory().add(prize);
					break;
				case GNOMESHAT_PINK: // gnome hats
				case GNOMESHAT_GREEN:
				case GNOMESHAT_PURPLE:
				case GNOMESHAT_CREAM:
				case GNOMESHAT_BLUE:
					otherPlayer.playerServerMessage(MessageType.QUEST, "You unwrap the present...");
					delay(unwrapDelay);
					otherPlayer.playerServerMessage(MessageType.QUEST, "it's a very nice pastel hat");
					otherPlayer.getCarriedItems().getInventory().add(prize);
					break;
				case GNOME_TOP_PINK: // gnome robe tops
				case GNOME_TOP_GREEN:
				case GNOME_TOP_PURPLE:
				case GNOME_TOP_CREAM:
				case GNOME_TOP_BLUE:
					otherPlayer.playerServerMessage(MessageType.QUEST, "You unwrap the present...");
					delay(unwrapDelay);
					otherPlayer.playerServerMessage(MessageType.QUEST, "it's a very nice pastel shirt");
					otherPlayer.getCarriedItems().getInventory().add(prize);
					break;


				case BOOTS_PINK: // gnome boots
				case BOOTS_GREEN:
				case BOOTS_PURPLE:
				case BOOTS_CREAM:
				case BOOTS_BLUE:
					otherPlayer.playerServerMessage(MessageType.QUEST, "You unwrap the present...");
					delay(unwrapDelay);
					otherPlayer.playerServerMessage(MessageType.QUEST, "oh! it's a pair of cute socks");
					otherPlayer.getCarriedItems().getInventory().add(prize);
					break;
				case DESERT_BOOTS:
					otherPlayer.playerServerMessage(MessageType.QUEST, "You unwrap the present...");
					delay(unwrapDelay);
					otherPlayer.playerServerMessage(MessageType.QUEST, "oh! it's a pair of socks...!");
					otherPlayer.getCarriedItems().getInventory().add(prize);
					break;

				case SPECIAL_CURRY_UNUSED:
					otherPlayer.playerServerMessage(MessageType.QUEST, "As you unwrap the present, you can smell something strange...");
					delay(unwrapDelay);
					otherPlayer.playerServerMessage(MessageType.QUEST, "it's a special christmas curry!!!");
					otherPlayer.getCarriedItems().getInventory().add(prize);
					delay(readingDelay);
					otherPlayer.playerServerMessage(MessageType.QUEST, "I wonder how they made it?"); // reference to it being unobtainable
					break;
				case GNOME_BATTA_UNUSED:
					otherPlayer.playerServerMessage(MessageType.QUEST, "As you unwrap the present, you can smell something weird...");
					delay(unwrapDelay);
					otherPlayer.playerServerMessage(MessageType.QUEST, "it's a homemade gnome batta... kind of smells like pants");
					otherPlayer.getCarriedItems().getInventory().add(prize);
					delay(readingDelay);
					otherPlayer.playerServerMessage(MessageType.QUEST, "I wonder how they made it?"); // reference to it being unobtainable
					break;


				default:
					// should not be reached
					otherPlayer.playerServerMessage(MessageType.QUEST, "You unwrap the present...");
					delay(unwrapDelay);
					otherPlayer.playerServerMessage(MessageType.QUEST, "oh! it's a " + prizeName + "!");
					otherPlayer.getCarriedItems().getInventory().add(prize);
					break;
			}
		} else {*/
			// TODO: Should port some of the above unique dialogues & behaviour
			// but in 2020, this code is unreachable on any Open RSC hosted server
			player.playerServerMessage(MessageType.QUEST, "You unwrap the present...");
			delay(unwrapDelay);
			if(player.getCarriedItems().remove(new Item(ItemId.PRESENT.id(), 1)) == -1) return;
			player.getCarriedItems().getInventory().add(prize);
			player.playerServerMessage(MessageType.QUEST, "oh! it's a " + prizeName + "!");
		//}
	}

	private void openRSCRollAndAwardPresent(Player player, Player otherPlayer) {
		openRSCRollAndAwardPresent(player, otherPlayer, false);
	}

	private void openRSCRollAndAwardPresent(Player player) {
		openRSCRollAndAwardPresent(player, player, true);
	}

}
